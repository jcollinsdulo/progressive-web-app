var cacheName = 'weatherPWA-v2.0';
var dataCacheName = 'weatherData-v2.0';
var filesToCache = [
    '/',
    '/index.html',
    '/favicon.ico',
    '/styles/ud811.css',
    '/scripts/app.js',
    '/scripts/localforage-1.4.0.js',
    '/images/clear.png',
    '/images/cloudy.png',
    '/images/cloudy_s_sunny.png',
    '/images/cloudy-scattered-showers.png',
    '/images/fog.png',
    '/images/ic_add_white_24px.png',
    '/images/ic_refresh_white_24px.png',
    '/images/partly-cloudy.png',
    '/images/rain.png',
    '/images/scattered-showers.png',
    '/images/sleet.png',
    '/images/snow.png',
    '/images/thunderstorm.png',
    '/images/wind.png',
];

//install event that caches all the files neeed by the app shell
self.addEventListener('install', function(e) {
    e.waitUntil(
        caches.open(cacheName).then(function(cache) {
            console.log('[ServiceWorker] Caching app shell');
            return cache.addAll(filesToCache);
        })
    );
});

// purge all the old files
self.addEventListener('activate', function (e) {
    console.log('[ServiceWorker] Activate');
    e.waitUntil(
        caches.keys().then(function (keyList) {
            return Promise.all(keyList.map(function (key) {
                if (key !== cacheName && key !== dataCacheName) {
                    console.log('[ServiceWorker] Removing old cache', key);
                    return caches.delete(key)
                    
                }
            }));
        })
    );
});

//fetch listener tries to get data from the cache, if none
//goes to get it from the network
self.addEventListener('fetch', function (e) {
    if (e.request.url.startsWith(weatherAPIUrlBase)) {
        e.respondWith(
            fetch(e.request).then(function (response) {
                return caches.open(dataCacheName).then(function (cache) {
                    cache.put(e.request.url, response.clone());
                    console.log('[ServiceWorker] Fetched & Cached', e.request.url);
                    return response;                    
                });
            })
        );
    } else {
        e.respondWith(
            caches.match(e.request).then(function (response) {
                console.log('[ServiceWorker] Fetch Only', e.request.url);
                return response || fetch(e.request);                
            })
        );
    }
});