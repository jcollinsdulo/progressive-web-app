#Dummy Weather Appliation - Progressive Web App Project

This project is based on progressive web app. PWAs as they are commonly known, offer content to the user whether online or offline.

PWAs take an offline first approach. In that, a user should be able to view content on an app whether connected or not. Also, a user can be able to add the web app to the home screen together with an icon just like a native app. Here, I build a dummy weather app that is a progressive web app.

You can find a live version here https://weatherappso.firebaseapp.com/